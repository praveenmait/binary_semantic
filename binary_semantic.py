#!/usr/bin/python

import sys
import os
from operator import itemgetter
from csv import reader
import csv

## This will takes input as csv file output as csv file.
## sys.argv command line argument

print "--------------------------------------------------"

try:
    inputcsvfilepath=sys.argv[1]    ## takes sys.argv[1] input csv file.
    print "INPUT CSV_FILE - %s"%(inputcsvfilepath)
except :
    print "input csv file path not exists"  ## execute command 'python binary_semantic.py /path/of/input/csv/file.csv outputfilename.csv'"
    print "--------------------------------------------------"
    exit(1)
try:
    outputcsvfilepath=sys.argv[2]  ## returns sys.argv[2] output csv file.
    print "OUTPUT CSV_FILE - %s"%(outputcsvfilepath)
except:
    outputcsvfilepath="output.csv"   ## returns if sys.argv[2] not given takes default value output.csv
    print "OUTPUT CSV_FILE (default) - %s"%(outputcsvfilepath)

with open(inputcsvfilepath) as fil:  ## open csv file  
    columns = zip(*reader(fil))	     ## Read file and create tuple of each column inside csv
    year, month = columns[:2]	     ## stores first two column as year and month
    data = columns[2:]		     ## store remaining columns in data
try:
    os.remove(outputcsvfilepath)     ## remove output csv file if exists
except OSError:
    pass

for shareVal in data:		     ## iterate over the data
    name = shareVal[0]
    try:
        share = map(int, shareVal[1:])
    except:
        print "There is error in CSV please correct error and then execute command again"
        print "--------------------------------------------------"
        exit(1)
    i, share = max(enumerate(share), key=itemgetter(1))	## get max value from the share value
    with open(outputcsvfilepath, 'a') as fil:		## open output csv file as in append mode
        writeobj = csv.writer(fil, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL) 
        print "%s, %s, %s %s"%(name, year[i+1], month[i+1],share)
        writeobj.writerow([name, year[i+1], month[i+1],share ])  ## write max value with year and month in a csv file.
print "--------------------------------------------------"
exit(1)
